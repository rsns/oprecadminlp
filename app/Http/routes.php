<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/cr/admin', function () {
    return view('welcomeadmin');
});

Route::get('/', 'GeneralController@index');

Route::controllers([
  'auth'=> 'Auth\AuthController',
  'password' => 'Auth\PasswordController'
]);

Route::get('terms', function(){
   return view('auth.terms');
});
//
//Route::group(['middleware'=>'auth', 'before' => 'member'], function(){
//  Route::resource('order','OrderController');
//});
