<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
          'firstname.required' => 'Firstname field is required',
            'lastname.required' => 'Lastname field is required',
            'nrp.digits_between' => 'NRP field must definitely 10 digits',
            'contact.required' => 'Phone number filed is required',
            'email.unique' => "Woops! The email you've entered is used for another application",
            'filecv.required' => 'Something wrong with your attachment, make sure it is a PDF and less than 3MBs',
	    'nrp.max' => 'Student Registration Number violation - Only the member of TC2014 is allowed to submit the application',
	    'nrp.min' => 'Student Registration Number violation - Only the member of TC2014 is allowed to submit the application',
        ];
        return Validator::make($data, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'nrp' => 'required|numeric|digits_between:10,10|max:5114100706|min:5114100001',
            'contact' => 'required|numeric',
            'email' => 'required|email',
            'filecv' => 'mimes:pdf|max:3024|required'
        ], $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(arrayterms $data)
    {
        return User::create($data);
    }

    protected function login(Request $request){
      $this->validate($request, ['username'=>'required','password'=>'required']);

      $identity = [
        'email'=> trim($request->get('username')),
        'password'=> trim($request->get('password')) ];

      // $remember = $request->has('remember');

      if($this->auth->attempt($identity, false))
      {
        return redirect()->intended('/');
      }

      //if data invalid, didnt return
      return redirect()->back()->withErrors('Username yang anda masukkan salah!')->withInput();
    }
}
