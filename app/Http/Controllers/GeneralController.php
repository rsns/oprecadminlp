<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use App\User;

class GeneralController extends Controller
{

    public function index()
    {
        return view('public.index');
    }

    public function register(Request $request, Input $input)
    {
        if($request->isMethod('get')){
          return view('public.register');
        }
        elseif($request->isMethod('post'))
        {
            $data = $request->only('firstname','lastname', 'email','contact', 'password');
            User::create([
              'firstname' => $data['firstname'],
              'lastname' => $data['lastname'],
              'email' => $data['email'],
              'contact' => $data['contact'],
              'password' => bcrypt($data['password'])
              ]);
            return redirect('/');
        }
        else {
          return view('public.register');
        }
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
