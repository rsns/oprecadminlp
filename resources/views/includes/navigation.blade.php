<!--Navigation-->
 <div class="navbar-fixed" >
    <nav id="nav_f" class="default_color" role="navigation" id='id-navbar'>
        <div class="container">
            <div class="nav-wrapper">
            <a href="{{ url() }}" id="logo-container" class="brand-logo"><img class='brand-logo valign' style='top:10%;height:80%'src="{{ url('logo.png') }}">
               </a>
                <span class="hide-on-med-and-down brand-logo" style="left:5%;">Programming Laboratory</span>
                <ul class="right hide-on-med-and-down">
                    {{--<li><a class="modal-trigger btn blue lighten-2" href='auth/login' href="#modal1">Log In</a>--}}
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<div id="modal1" class="modal s12 m6 l6">
    <div class="modal-content">
      <h4>Modal Header</h4>
      <p>A bunch of text</p>
        <div class="input-field">
          <i class='material-icons prefix'>account_circle</i>
          <input name="email" type="email" class="validate">
          <label>Email</label>
        </div>
        <div class="input-field">
          <i class='material-icons prefix'>vpn_key</i>
          <input name="password" type="password">
          <label>Password</label>
        </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
    </div>
  </div>
