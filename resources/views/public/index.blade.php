<!DOCTYPE html>
<html>
<head>

	  @include('includes.header')


		<style>
			#pamer{
				height:50%;
			}
		</style>
</head>
<body id="top" class="scrollspy">

	@include('includes.preloader')

	@include('includes.navigation')

		<div id="intro" class="section scrollspy">
		    <div class="container">
				<div class="row">
					<div class="col s12 m12 l6" style="padding-top:9%;">
						<img src="{{url('cewek.png')}}" class="col s6 m6 l6 left circle ">
						<img src="{{url('cowok.png')}}"  class="col s6 m6 l6 right circle">
					</div>
					<div class="col s12 m12 l6">
						<h1 class="text_h center header no-pad-bot">
							Come to <br><strong>join us</strong>
							<br>
							<img class="center" src="{{url('logo.png')}}" style="width:2em">
						</h1>
					</div>
                    <div  class="col s12">
                        <h2 class="center header">
                            Open Recruitment <b>Programming Laboratory</b> Administrator 2016
                        </h2>
						<h5 class="no-pad-top center">
							<blockquote>
								Deadline: Saturday,2016 4th March
							</blockquote>
						</h5>
                        <div class="center-align">
                            <a class="waves-effect waves-light btn-large center-align flow-text" href="{{url('terms')}}">
                                <i class="material-icons right">done</i> Yes, I'm coming!
                            </a>
                        </div>
                    </div>

		    </div>
		</div>

		@include('includes.footer')
    <!--  Scripts-->
    <script src="{{ url('assets/material')}}/min/plugin-min.js"></script>
    <script src="{{ url('assets/material')}}/min/custom-min.js"></script>
    <!-- <script src="{{ url('assets/js/jquery.fullpage.min.js')}}"></script> -->


			<script type="text/javascript">
					$(document).ready(function() {
						$('.slider').slider({
							indicators: false,

							});

						 $('.modal-trigger').leanModal();
					});
			</script>
    </body>
</html>
