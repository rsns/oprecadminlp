@extends('layouts.public')
@section('title')
    Application Form
@endsection

@section('page-content')

    <div class="container">
        <div id="forms" class="col s12">
            <div class='container '>
                <div class="row">
                    <form class="col s12  " action="" method="post" enctype="multipart/form-data">
                        <h2 class='header'> Application Form </h2>
                        <p><strong>Programming Laboratory</strong> New Administrators Application </p>
                        @if ($errors->has())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="row">
                            <div class="input-field col s12">
                                <i class='material-icons prefix'>perm_identity</i>
                                <input name="nrp" type="number" class="validate">
                                <label>Student ID Number / NRP </label>
                            </div>
                        </div>
                        <div class="row">
                            {{ csrf_field() }}
                            <div class="input-field col s6">
                                <i class='material-icons prefix'>account_circle</i>
                                <input placeholder="Nama anda.. " name="firstname" type="text" class="validate">
                                <label for="first_name">First Name</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="last_name" type="text" class="validate" name='lastname' >
                                <label for="last_name">Last Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class='material-icons prefix'>email</i>
                                <input name="email" type="email" class="validate">
                                <label>Email Address</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class='material-icons prefix'>contact_phone</i>
                                <input id="email" type="number" name='contact' class="validate" >
                                <label>Phone Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class='material-icons prefix'>languange</i>
                                <input id="linkfb" type="text" name='linkfb' class="validate">
                                <label>Your facebook profile links</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="file-field input-field col s12">
                                <p><strong>
                                        <i class="material-icons">info_outline</i>
                                        The allowed attachment is only with .pdf extension and maximum size of 3MB.
                                    </strong>
                                </p>
                                <div class="btn">
                                    <span>File CV</span>
                                    <input type="file"   name="filecv" required>
                                </div>
                                <div class="file-path-wrapper">
                                    <input type="text"  class="file-path validate" placeholder="Upload your CV here" required>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-large waves-light red right  darken-1" type="submit" name="action">Apply
                            <i class="material-icons right">send</i>
                        </button>
                    </form>

                </div>

            </div>
        </div>
    </div>

@stop

@section('custom-scripts')

    <script type="javascript" href="{{ url('jquery-2.1.4.min.js')}}"></script>
    <script type="javascript" href="{{ url('materialize.js')}}"></script>
    <script department='text/javascript'>
        $(document).ready(function(){

            $('.modal-trigger').leanModal();

        });
    </script>
@stop
