<!DOCTYPE html>
<html>
<head>

    @include('includes.header')
    @yield('custom-head')

    <style>
        #pamer{
            height:50%;
        }
    </style>
</head>
<body id="top" class="scrollspy">

@include('includes.preloader')

@include('includes.navigation')

<!--Perkenalan intro, visi misi -->
<div id="intro" class="section scrollspy">
    <div class="container">
        <div class="row">
                <h2 class="center header text_h2 ">
                    Thank you {{ Session::get('user') }} for applying!
                    <br>
                    <span class="flow-text">

                        We will contact you soon.
                    </span>
                </h2>
                <h3 class="center header text_h3">
                </h3>

            <br>
            <div class="s12">

                <h1 class=" flow-text center" style="font-size:2.5rem; font-family:'Vollkorn';font-style: normal;font-weight: 400;">
                    <i> <strong>
                            "Respect is earned. Honesty is apreciated. <br>Love is gained. Loyalty is returned."
                    </i></strong>
                </h1>
            </div>

        </div>
    </div>
</div>
    @include('includes.footer')
            <!--  Scripts-->
    <script src="{{ url('assets/material')}}/min/plugin-min.js"></script>
    <script src="{{ url('assets/material')}}/min/custom-min.js"></script>
    <!-- <script src="{{ url('assets/js/jquery.fullpage.min.js')}}"></script> -->


    <script type="text/javascript">
        $(document).ready(function() {
            $('.slider').slider({
                indicators: false,

            });

            $('.modal-trigger').leanModal();
        });
    </script>
</body>
</html>
